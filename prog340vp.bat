@ECHO OFF
@ECHO Please ensure that JAVA_HOME is set correctly and that both java.exe and javac.exe are in your PATH.
@ECHO Compiling Java Source files...
javac -Xlint:unchecked src/edu/metrostate/ics340/graph/Edge.java src/edu/metrostate/ics340/graph/Node.java src/edu/metrostate/ics340/graph/Graph.java src/edu/metrostate/ics340/Consat.java src/edu/metrostate/ics340/FeatureBase.java src/edu/metrostate/ics340/Heap.java src/edu/metrostate/ics340/Io.java src/edu/metrostate/ics340/Mist.java src/edu/metrostate/ics340/Sale1.java src/edu/metrostate/ics340/Sale2.java src/edu/metrostate/ics340/Prog340VP.java
@ECHO Running Prog340VP...
java -cp ./src edu.metrostate.ics340.Prog340VP
@ECHO done.
