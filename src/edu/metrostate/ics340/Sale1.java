/**
 * File: Sale1.java
 */
package edu.metrostate.ics340;

import edu.metrostate.ics340.graph.Graph;

/**
 * <p>
 * Sale1 class for ICS 340 Programming Assignment.
 * <p>
 * The Sale1 class solves the Traveling Salesperson Problem using the graph.
 * 
 * @author Vincent J Palodichuk <a HREF="mailto:hu0011wy@metrostate.edu">
 *         (e-mail me) </a>
 *
 * @version 01/12/2018
 */ 
public class Sale1 extends FeatureBase{

    /**
     * Constructs a new Sale1 with the specified input file and graph.
     * @param file The input file.
     * @param graph The graph.
     */
    public Sale1(String file, Graph<String> graph) {
        super(file, graph);
    }
}
