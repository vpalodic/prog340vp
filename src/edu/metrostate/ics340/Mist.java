/**
 * File: Mist.java
 */
package edu.metrostate.ics340;

import edu.metrostate.ics340.graph.Graph;

/**
 * <p>
 * Mist class for ICS 340 Programming Assignment.
 * <p>
 * The Mist class computes the minimum spanning tree of the a connected graph
 * using a Heap.
 * 
 * @author Vincent J Palodichuk <a HREF="mailto:hu0011wy@metrostate.edu">
 *         (e-mail me) </a>
 *
 * @version 01/12/2018
 */ 
public class Mist extends FeatureBase {

    /**
     * Constructs a new Mist with the specified input file and graph.
     * @param file The input file.
     * @param graph The graph.
     */
    public Mist(String file, Graph<String> graph) {
        super(file, graph);
    }
}
