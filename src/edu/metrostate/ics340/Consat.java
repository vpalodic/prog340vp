/**
 * File: Consat.java
 */
package edu.metrostate.ics340;

import edu.metrostate.ics340.graph.Graph;

/**
 * <p>
 * Consat class for ICS 340 Programming Assignment.
 * <p>
 * The Consat class solves constraint satisfaction problems using the graph.
 * 
 * @author Vincent J Palodichuk <a HREF="mailto:hu0011wy@metrostate.edu">
 *         (e-mail me) </a>
 *
 * @version 01/12/2018
 */ 
public class Consat extends FeatureBase {

    /**
     * Constructs a new Consat with the specified input file and graph.
     * @param file The input file.
     * @param graph The graph.
     */
    public Consat(String file, Graph<String> graph) {
        super(file, graph);
    }
}
