/**
 * File: Prog340VP.java
 */
package edu.metrostate.ics340;

import edu.metrostate.ics340.graph.Graph;
import java.io.File;
import java.util.Objects;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToolBar;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

/**
 * <p>
 * Main JavaFX Application class for ICS 340 Programming assignment.
 * 
 * The Prog340VP class is responsible for loading graphs from text files
 * and executing the six features of the program.
 * <p>
 * <b>Features:</b>
 * <ul>
 *  <li>Io - Processes and writes details about a graph to a text file.</li>
 *  <li>Heap - Turns a graph with nodes, but not edges into a heap and removes
 *  items from it.</li>
 *  <li>Mist - Computes the minimum spanning tree of a connected graph using a Heap.</li>
 *  <li>Sale 1 - Solves the Traveling Salesperson Problem on input graphs.</li>
 *  <li>Sale 2 - Solves the Traveling Salesperson Problem using a different algorithm.</li>
 *  <li>Consat - Solves constraint satisfaction problems.</li>
 * </ul>
 * 
 * @author Vincent J Palodichuk <a HREF="mailto:hu0011wy@metrostate.edu">
 *         (e-mail me) </a>
 *
 * @version 01/11/2018
 *
 */
public class Prog340VP extends Application implements EventHandler<ActionEvent> {
    private static final String APP_TITLE = "Prog340VP";
    private static final String GRAPH_NAME_FMT = "Graph: %s";
    private static final String GRAPH_NUM_NODES_FMT = "Nodes: %s";
    private static final String GRAPH_NUM_EDGES_FMT = "Edges: %s";
    private static final int DEFAULT_WIDTH = 600;
    private static final int DEFAULT_HEIGHT = 400;
    private static final String MENU_FILE = "File";
    private static final String MENU_ACTION = "Action";
    private static final String MENU_HELP = "Help";
    private static final String ACTION_CONSAT = "Consat";
    private static final String ACTION_SALE_2 = "Sale 2";
    private static final String ACTION_SALE_1 = "Sale 1";
    private static final String ACTION_MIST = "Mist";
    private static final String ACTION_HEAP = "Heap";
    private static final String ACTION_IO = "Io";
    private static final String ACTION_ABOUT = "About";
    private static final String ACTION_EXIT = "Exit";
    private static final String ACTION_CLEAR_OUTPUT = "Clear Output";
    private static final String ACTION_OPEN_GRAPH = "Open Graph...";
    private static final String MSG_EXECUTING_FMT = "%nExecuting %s feature...%n";
    
    private final Label graphName = new Label();
    private final Label numNodes = new Label();
    private final Label numEdges = new Label();
    private final TextArea output = new TextArea();
    private final Menu fileMenu = new Menu(MENU_FILE);
    private final Menu actionMenu = new Menu(MENU_ACTION);
    private final Menu helpMenu = new Menu(MENU_HELP);
    private ToolBar toolBar = null;
    private Stage stage = null;
    private File selectedFile = null;
    private Graph<String> graph = null;
    
    /**
     * Sets up the toolbar for this application.
     * @param actionEvent The event handler that will handle the toolbar actions.
     * @return the javafx.scene.control.ToolBar setup with buttons.
     */
    private ToolBar setupToolBar(EventHandler<ActionEvent> actionEvent) {
        ToolBar answer = null;
        
        Button open = new Button(ACTION_OPEN_GRAPH);
        open.setOnAction(actionEvent);
        open.setId(ACTION_OPEN_GRAPH);
        
        Button clear = new Button(ACTION_CLEAR_OUTPUT);
        clear.setOnAction(actionEvent);
        clear.setId(ACTION_CLEAR_OUTPUT);
        
        Button exit = new Button(ACTION_EXIT);
        exit.setOnAction(actionEvent);
        exit.setId(ACTION_EXIT);
        
        Button io = new Button(ACTION_IO);
        io.setOnAction(actionEvent);
        io.setId(ACTION_IO);
        
        Button heap = new Button(ACTION_HEAP);
        heap.setOnAction(actionEvent);
        heap.setId(ACTION_HEAP);
        
        Button mist = new Button(ACTION_MIST);
        mist.setOnAction(actionEvent);
        mist.setId(ACTION_MIST);
        
        Button sale1 = new Button(ACTION_SALE_1);
        sale1.setOnAction(actionEvent);
        sale1.setId(ACTION_SALE_1);
        
        Button sale2 = new Button(ACTION_SALE_2);
        sale2.setOnAction(actionEvent);
        sale2.setId(ACTION_SALE_2);
        
        Button consat = new Button(ACTION_CONSAT);
        consat.setOnAction(actionEvent);
        consat.setId(ACTION_CONSAT);
        
        answer = new ToolBar(open, clear, new Separator(Orientation.HORIZONTAL), exit,
        new Separator(Orientation.HORIZONTAL), io, heap, mist, sale1, sale2, consat);
        
        return answer;
    }
    
    /**
     * Sets up the menus for this application.
     * @param actionEvent The event handler that will handle the menu actions.
     * @return the javafx.scene.control.MenuBar setup with buttons.
     */
    private MenuBar setupMenu(EventHandler<ActionEvent> actionEvent) {
        /* File menu items: */
        MenuItem open = new MenuItem(ACTION_OPEN_GRAPH);
        open.setAccelerator(KeyCombination.keyCombination("Ctrl+O"));
        open.setOnAction(actionEvent);
        open.setId(ACTION_OPEN_GRAPH);
        
        MenuItem clear = new MenuItem(ACTION_CLEAR_OUTPUT);
        clear.setAccelerator(KeyCombination.keyCombination("Ctrl+W"));
        clear.setOnAction(actionEvent);
        clear.setId(ACTION_CLEAR_OUTPUT);
        
        MenuItem exit = new MenuItem(ACTION_EXIT);
        exit.setAccelerator(KeyCombination.keyCombination("Alt+F4"));
        exit.setOnAction(actionEvent);
        exit.setId(ACTION_EXIT);
        
        fileMenu.getItems().addAll(open, new SeparatorMenuItem(), clear, new SeparatorMenuItem(), exit);
        
        /* Action menu items: */
        MenuItem ioItem = new MenuItem (ACTION_IO);
        ioItem.setAccelerator(KeyCombination.keyCombination("Ctrl+I"));
        ioItem.setOnAction(actionEvent);
        ioItem.setId(ACTION_IO);
        
        MenuItem heapItem = new MenuItem (ACTION_HEAP);
        heapItem.setAccelerator(KeyCombination.keyCombination("Ctrl+E"));
        heapItem.setOnAction(actionEvent);
        heapItem.setId(ACTION_HEAP);
        
        MenuItem mistItem = new MenuItem (ACTION_MIST);
        mistItem.setAccelerator(KeyCombination.keyCombination("Ctrl+M"));
        mistItem.setOnAction(actionEvent);
        mistItem.setId(ACTION_MIST);
        
        MenuItem sale1Item = new MenuItem (ACTION_SALE_1);
        sale1Item.setAccelerator(KeyCombination.keyCombination("Ctrl+S"));
        sale1Item.setOnAction(actionEvent);
        sale1Item.setId(ACTION_SALE_1);
        
        MenuItem sale2Item = new MenuItem (ACTION_SALE_2);
        sale2Item.setAccelerator(KeyCombination.keyCombination("Ctrl+T"));
        sale2Item.setOnAction(actionEvent);
        sale2Item.setId(ACTION_SALE_2);
        
        MenuItem consatItem = new MenuItem (ACTION_CONSAT);
        consatItem.setAccelerator(KeyCombination.keyCombination("Ctrl+N"));
        consatItem.setOnAction(actionEvent);
        consatItem.setId(ACTION_CONSAT);
        
        actionMenu.getItems().addAll(ioItem, heapItem, mistItem, sale1Item, sale2Item, consatItem);
        
        /* Help menu items: */
        MenuItem about = new MenuItem(ACTION_ABOUT);
        about.setAccelerator(KeyCombination.keyCombination("Ctrl+F1"));
        about.setOnAction(actionEvent);
        about.setId(ACTION_ABOUT);
        
        helpMenu.getItems().addAll(about);
        
        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().addAll(fileMenu, actionMenu, helpMenu);
        return menuBar;
    }

    /**
     * Called in the event handler to display information about this program.
     */
    public void aboutProgram() {
        output.appendText("\n\nAbout this program:\n");
        output.appendText("Metro State University\n");
        output.appendText("ICS 340\n");
        output.appendText("Programming Assignment\n");
        output.appendText("by Vincent J. Palodichuk\n");
    }

    /**
     * Displays the Open File Dialog box to select a graph file to open an process.
     * If a file is selected from the Open File Dialog, the selectedFile is updated
     * with the file the user selected and the file is loaded in to a new graph.
     * 
     * @param stage The stage that owns the File Open Dialog box.
     */
    public void openGraph(Stage stage) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Graph File");
        fileChooser.getExtensionFilters().addAll(
                new ExtensionFilter("Text Files", "*.txt"),
                new ExtensionFilter("All Files", "*.*"));
        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir"))); // user.dir is the directory the JVM was executed from.
        File file = fileChooser.showOpenDialog(stage);
        
        if (file != null) {
            selectedFile = file;
            openGraphFile();
        }
    }
    
    /**
     * Processes the selected graph text file. If no file is currently selected,
     * an IllegalStateException will be thrown. If the currently selected file is
     * not a valid graph file then the current graph is not modified.
     * @throws IllegalStateException Indicates there is no selected file.
     */
    public void openGraphFile() {
        if (selectedFile == null) {
            throw new IllegalStateException("A file must be selected in order to activate this method.");
        }
        
        graph = Graph.stringGraphFromFile(selectedFile);
        
        if (graph != null) {
            setGraphName(graph.getName());
            setNumNodes(String.format("%d", graph.getNumNodes()));
            setNumEdges(String.format("%d", graph.getNumEdges()));
            output.clear();

            output.appendText(String.format("Graph %s has been read and contains %s nodes and %s edges.%n", graph.getName(), graph.getNumNodes(), graph.getNumEdges()));
            disableActions(false);
        } else {
            output.appendText("\n\nUnable to read the graph from " + selectedFile.getAbsolutePath() + "\n");
        }            
    }

    /**
     * Disables or Enables the GUI controls for the available actions of this program.
     * 
     * @param disable If true, the GUI action controls are disabled. If false,
     * the GUI action controls are enabled.
     * 
     */
    public void disableActions(boolean disable) {
        if (actionMenu != null) {
            actionMenu.getItems().forEach((item) -> {
                item.setDisable(disable);
            });
        }

        if (toolBar != null) {        
            toolBar
                .getItems()
                .forEach((node) -> {
                if (node instanceof Button) {
                    Button btn = (Button) node;
                    if ((Objects.equals(btn.getText(), ACTION_IO))
                            || (Objects.equals(btn.getText(), ACTION_HEAP))
                            || (Objects.equals(btn.getText(), ACTION_MIST))
                            || (Objects.equals(btn.getText(), ACTION_SALE_1))
                            || (Objects.equals(btn.getText(), ACTION_SALE_2))
                            || (Objects.equals(btn.getText(), ACTION_CONSAT))) {
                        btn.setDisable(disable);
                    }
                }
            });
        }
    }
    
    /**
     * Setups the labels that the application uses to display information about
     * the current graph.
     */
    private void setupLabelControls() {
        setGraphName("*** No file loaded ***");
        setNumNodes("N/A");
        setNumEdges("N/A");
        
        output.setEditable(false);
        output.setWrapText(true);
    }
    
    /**
     * Updates the name label with the name of the graph.
     * @param name The name of the graph.
     */
    public void setGraphName(String name) {
        stage.setTitle(String.format("%s - %s", APP_TITLE, name));
        graphName.setText(String.format(GRAPH_NAME_FMT, name));
    }
    
    /**
     * Updates the nodes label with the number of nodes in the current graph.
     * @param nodes The number of nodes as a java.lang.String
     */
    public void setNumNodes(String nodes) {
        numNodes.setText(String.format(GRAPH_NUM_NODES_FMT, nodes));
    }
    
    /**
     * Updates the edges label with the number of edges in the current graph.
     * @param edges The number of edges as a java.lang.String
     */
    public void setNumEdges(String edges) {
        numEdges.setText(String.format(GRAPH_NUM_EDGES_FMT, edges));
    }
    
    /**
     * Setups the stage and initial state of the UI for the application.
     * @param theStage The stage to be prepared.
     */
    @Override
    public void start(Stage theStage) {
        stage = theStage;
        stage.setTitle(APP_TITLE);
        VBox root = new VBox();
        Scene scene = new Scene(root, DEFAULT_WIDTH, DEFAULT_HEIGHT);
        scene.setFill(Color.VIOLET);
        
        MenuBar menu = setupMenu(this);
        toolBar = setupToolBar(this);
        disableActions(true);
        
        setupLabelControls();
        
        HBox hbox = new HBox();
        hbox.setAlignment(Pos.CENTER);
        hbox.setSpacing(10);
        hbox.setPadding(new Insets(0, 10, 0, 10));
        hbox.getChildren().addAll(numNodes, numEdges);
        VBox vbox = new VBox();
        vbox.setAlignment(Pos.CENTER);
        vbox.setSpacing(10);
        vbox.setPadding(new Insets(0, 10, 0, 10));
        vbox.getChildren().addAll(graphName, hbox, output);
        root.getChildren().addAll(menu, toolBar, vbox);
        output.setMinHeight(DEFAULT_HEIGHT - 135);
        
        output.appendText("Before you can perform any actions, please open a Graph file by clicking the File menu and selecting Open or press Ctrl + O.");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }
    
    /**
     * Main entry point of the application.
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Handles events for UI Controls such as MenuItems and Buttons.
     * @param actionEvent The event object that describes the event being handled.
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        final Object source = actionEvent.getSource();
        
        if (source instanceof Control || source instanceof MenuItem) {
            String id = "";
            
            if (source instanceof Control) {
                id = ((Control) source).getId();
            } else if (source instanceof MenuItem) {
                id = ((MenuItem) source).getId();
            }
            
            switch (id) {
                case ACTION_OPEN_GRAPH:
                    openGraph(stage);
                    break;
                case ACTION_CLEAR_OUTPUT:
                    output.clear();
                    break;
                case ACTION_EXIT:
                    Platform.exit();
                    break;
                case ACTION_ABOUT:
                    aboutProgram();
                    break;
                case ACTION_IO:
                    output.appendText(String.format(MSG_EXECUTING_FMT, ACTION_IO));
                    Io io = new Io(selectedFile.getAbsolutePath(), graph);
                    output.appendText(io.execute());
                    break;
                case ACTION_HEAP:
                    output.appendText(String.format(MSG_EXECUTING_FMT, ACTION_HEAP));
                    Heap heap = new Heap(selectedFile.getAbsolutePath(), graph);
                    output.appendText(heap.execute());
                    break;
                case ACTION_MIST:
                    output.appendText(String.format(MSG_EXECUTING_FMT, ACTION_MIST));
                    Mist mist = new Mist(selectedFile.getAbsolutePath(), graph);
                    output.appendText(mist.execute());
                    break;
                case ACTION_SALE_1:
                    output.appendText(String.format(MSG_EXECUTING_FMT, ACTION_SALE_1));
                    Sale1 sale1 = new Sale1(selectedFile.getAbsolutePath(), graph);
                    output.appendText(sale1.execute());
                    break;
                case ACTION_SALE_2:
                    output.appendText(String.format(MSG_EXECUTING_FMT, ACTION_SALE_2));
                    Sale2 sale2 = new Sale2(selectedFile.getAbsolutePath(), graph);
                    output.appendText(sale2.execute());
                    break;
                case ACTION_CONSAT:
                    output.appendText(String.format(MSG_EXECUTING_FMT, ACTION_CONSAT));
                    Consat consat = new Consat(selectedFile.getAbsolutePath(), graph);
                    output.appendText(consat.execute());
                    break;
            }
        }
    }
}
