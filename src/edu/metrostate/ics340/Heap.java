/**
 * File: Heap.java
 */
package edu.metrostate.ics340;

import edu.metrostate.ics340.graph.Graph;

/**
 * <p>
 * Heap class for ICS 340 Programming Assignment.
 * <p>
 * The Heap class turns a graph with nodes but no edges into a heap and removes
 * items from it.
 * 
 * @author Vincent J Palodichuk <a HREF="mailto:hu0011wy@metrostate.edu">
 *         (e-mail me) </a>
 *
 * @version 01/12/2018
 */ 
public class Heap extends FeatureBase {
    /**
     * Constructs a new Heap with the specified input file and graph.
     * @param file The input file.
     * @param graph The graph.
     */
    public Heap(String file, Graph<String> graph) {
        super(file, graph);
    }
}
