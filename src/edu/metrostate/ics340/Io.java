/**
 * File: Io.java
 */
package edu.metrostate.ics340;

import edu.metrostate.ics340.graph.Graph;
import edu.metrostate.ics340.graph.Node;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>
 * Io class for ICS 340 Programming Assignment.
 * <p>
 * The Io class processes a graph and then saves the generated report to a text
 * file in the same directory as the source file. The output file has the same
 * name as the input with _out appended before the extension. The report is also
 * returned from the execute method as a String.
 * 
 * @author Vincent J Palodichuk <a HREF="mailto:hu0011wy@metrostate.edu">
 *         (e-mail me) </a>
 *
 * @version 01/12/2018
 */ 
public class Io extends FeatureBase {
    
    /**
     * Constructs a new Io with the specified input file and graph.
     * @param fullPath The complete input file name and path.
     * @param graph The graph.
     */
    public Io(String fullPath, Graph<String> graph) {
        super(fullPath, graph);
    }

    /**
     * Processes the graph and saves the report to a file in the same directory as
     * the input file and with the same name as the input file with _out appended
     * before the extension. If the output file already exists, it is overwritten.
     * If the report is successfully saved to the file, 'Graph details saved.'
     * will be appended to the report that is returned. If the report was unable to
     * be written to the output file, then 'Graph details not saved.' will be
     * appended to the report that is returned.
     * 
     * @return A report of the graph.
     */
    @Override
    public String execute() {
        StringBuilder output = processGraph();
        
        if (writeOutput(output.toString())) {
            output.append("\nGraph details saved.");
        } else {
            output.append("\nGraph details not saved.");
        }
        
        return output.toString();
    }
    
    /**
     * Processes the graph and returns a StringBuilder that contains the report.
     * @return A StringBuilder that contains the report of the graph.
     * @throws IllegalStateException Indicates that this io has no graph.
     */
    private StringBuilder processGraph() {
        if (graph == null) {
            throw new IllegalStateException("A valid graph must be loaded first.");
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Graph: ").append(graph.getName()).append("\n");
        Set<String> mnemonics = graph.getNodeMnemonics();
        
        mnemonics.forEach((mnemonic) -> {
            sb.append("\n");
            processNode(sb, mnemonic, graph.getNode(mnemonic));
        });
        
        return sb;
    }

    /**
     * Processes the specified node and adds the report to the specified
     * StringBuilder.
     * @param sb The StringBuilder to add report information to. Must not be null.
     * @param node The node to process. Must not be null.
     */
    private void processNode(StringBuilder sb, String mnemonic, Node<String> node) {
        sb.append(String.format("Node %s, mnemonic %s, ", node.getName(), mnemonic));
        
        String value = node.getValue();
        
        if (value == null) {
            sb.append("no value\n");
        } else {
            sb.append(String.format("value %s%n", value));
        }
        
        graph.getEdges(mnemonic).forEach((edge) -> {
            sb.append(String.format("%s has edge to %s labeled %s%n", node.getName(), edge.getTarget().getName(), edge.getLabel()));
        });
        
        graph.getEdgesTo(mnemonic).forEach((edge) -> {
            sb.append(String.format("%s has edge from %s labeled %s%n", node.getName(), edge.getSource().getName(), edge.getLabel()));
        });
    }

    /**
     * Writes the specified report to a text file. Appends _out to the source file name
     * before the extension and uses that as the output file name.
     * @param output The report to save to a file. Must not be null.
     * @return True if the report was written to a file; otherwise false.
     * @throws IllegalStateException Indicates that the source file is null.
     * @throws IllegalArgumentException Indicates that the report is null.
     */
    private boolean writeOutput(String output) {
        boolean answer = false;
        
        File file = new File(filename);
        
        if (file.exists()) {
            file.delete();
        }
        
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(file));) {
            bw.write(output);
            answer = true;
        } catch (IOException ex) {
            Logger.getLogger(Io.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return answer;
    }
}
