/**
 * File: FeatureBase.java
 */
package edu.metrostate.ics340;

import edu.metrostate.ics340.graph.Graph;
import java.io.File;

/**
 * <p>
 * FeatureBase class for ICS 340 Programming Assignment. Provides base functionality
 * for all features of this program.
 * 
 * @author Vincent J Palodichuk <a HREF="mailto:hu0011wy@metrostate.edu">
 *         (e-mail me) </a>
 *
 * @version 01/14/2018
 *
 */
public abstract class FeatureBase {
    /**
     * The default message that is returned from the execute method if it is not
     * overridden by a derived class.
     */
    protected static final String MSG_NOT_IMPL_FMT = "%n%s feature has not been implemented yet.%n";
    
    /**
     * The suffix that is added to the end of input files to produce an output
     * filename.
     */
    protected static final String SUFFIX = "_out"; // suffix to append to output file.
    
    /**
     * The filename for the output file.
     */
    protected final String filename;
    
    /**
     * The graph to operate on.
     */
    protected final Graph<String> graph;
    
    /**
     * Constructs a new feature instance with the specified file and graph.
     * 
     * @param fullPath The full path to the file
     * @param graph The graph to operate on.
     */
    public FeatureBase(String fullPath, Graph<String> graph) {
        if (fullPath == null) {
            throw new IllegalStateException("fullPath cannot be null.");
        }
        
        if (graph == null) {
            throw new IllegalArgumentException("graph cannot be null.");
        }
        
        String ext = "";
        String name = "";
        
        int extIndex = fullPath.lastIndexOf('.');
        
        if (extIndex >= 0) {
            ext = fullPath.substring(extIndex);
        }
        
        int index = fullPath.lastIndexOf(File.separatorChar);
        
        if (index >= 0) {
            name = fullPath.substring(index + 1, extIndex >= 0 ? extIndex : fullPath.length());
        }
        
        String outFileName = name + SUFFIX + ext;
        
        String outPath = "";
        
        if (index >= 0) {
            outPath = fullPath.substring(0, index + 1);
        }
        
        filename = outPath + outFileName;
        this.graph = graph;
    }
    
    /**
     * Default implementation of the execute method returns a default not
     * implemented message.
     * 
     * @return The java.lang.String message.
     */
    public String execute() {
        String name = this.getClass().getName();
        
        int index = name.lastIndexOf('.');
        
        if (index >= 0) {
            name = name.substring(index + 1);
        }
        
        return String.format(MSG_NOT_IMPL_FMT, name);
    }
}
