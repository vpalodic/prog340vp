/**
 * File: Sale2.java
 */
package edu.metrostate.ics340;

import edu.metrostate.ics340.graph.Graph;

/**
 * <p>
 * Sale2 class for ICS 340 Programming Assignment.
 * <p>
 * The Sale2 class solves the Traveling Salesperson Problem using the graph but,
 * it uses a different algorithm than Sale1 does.
 * 
 * @author Vincent J Palodichuk <a HREF="mailto:hu0011wy@metrostate.edu">
 *         (e-mail me) </a>
 *
 * @version 01/12/2018
 */ 
public class Sale2 extends FeatureBase {

    /**
     * Constructs a new Sale2 with the specified input file and graph.
     * @param file The input file.
     * @param graph The graph.
     */
    public Sale2(String file, Graph<String> graph) {
        super(file, graph);
    }
}
