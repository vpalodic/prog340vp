/**
 * File: Node.java
 */
package edu.metrostate.ics340.graph;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * <p>
 * Node class for ICS 340 Programming Assignment that represents a node in a 
 * graph. A node has a name, mnemonic, value, and set of Edges that it connects
 * to.
 * 
 * @author Vincent J Palodichuk <a HREF="mailto:hu0011wy@metrostate.edu">
 *         (e-mail me) </a>
 *
 * @version 01/12/2018
 * 
 * @param <T> The type for the value and label of a Node and Edge respectively.
 */
public class Node<T extends Comparable<?>> {
    private String mnemonic;
    private String name;
    private T value;
    private Set<Edge<T>> edges;
    
    /**
     * Constructs an empty Node.
     */
    public Node() {
        this(null, null, null);
    }
    
    /**
     * Constructs an empty Node with the specified mnemonic.
     * @param mnemonic The identifier for this node.
     */
    public Node(String mnemonic) {
        this(mnemonic, null, null);
    }
    
    /**
     * Constructs a new Node with the specified mnemonic, name, and value.
     * @param mnemonic The identifier for this node.
     * @param name The name of this node
     * @param value The value for this node
     */
    public Node(String mnemonic, String name, T value) {
        this.mnemonic = mnemonic;
        this.name = name;
        this.value = value;
        
        edges = new HashSet<>();
    }    

    /**
     * Adds an edge to the specified target node with the specified label.
     * Returns true if the edge was added; otherwise false is returned.
     * @param target The target node of the edge.
     * @param label The label for the edge.
     * @return True if the edge was added; otherwise false is returned.
     */
    public boolean addEdge(Node<T> target, T label) {
        return edges.add(new Edge<>(this, target, label));
    }
    
    /**
     * Returns a set of edges for this node.
     * @return A java.util.Set&lt;edu.metrostate.ics340.graph.Edge&lt;T&gt;&gt; containing
     * the requested edges.
     */
    public Set<Edge<T>> getEdges() {
        return new HashSet<>(edges);
    }
    
    /**
     * Returns the number of edges this node has.
     * @return The number of edges this node has.
     */
    public int getNumEdges() {
        return edges.size();
    }
    
    /**
     * Removes all of the edges from this node.
     */
    public void removeEdges() {
        edges.clear();
    }
    
    /**
     * Removes the edge to the specified node if one exists. Returns a reference
     * to the edge that was removed or null if the edge does not exist.
     * @param target The target node of the edge that this node connects to.
     * @return Returns a reference to the edge that was removed or null if the
     * edge does not exist.
     */
    public Edge<T> removeEdge(Node<T> target) {
        Optional<Edge<T>> opt = edges.stream()
                .filter((edge) -> Objects.equals(target, edge.getTarget()))
                .findFirst();
        
        Edge<T> answer = null;
        
        if (opt.isPresent()) {
            answer = opt.get();
        }
        
        if (edges.contains(answer)) {
            edges.remove(answer);
        }
        
        return answer;
    }
    
    /**
     * Returns the identifier of this node.
     * @return The  identifier of this node.
     */
    public String getMnemonic() {
        return mnemonic;
    }

    /**
     * Returns the name of this node.
     * @return The name of this node.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the value of this node.
     * @return The value of this node.
     */
    public T getValue() {
        return value;
    }

    /**
     * Sets the new identifier of this node.
     * @param mnemonic The new identifier of this node.
     */
    public void setMnemonic(String mnemonic) {
        this.mnemonic = mnemonic;
    }

    /**
     * Sets the new name of this node.
     * @param name The new name of this node.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the new value of this node.
     * @param value The new value of this node.
     */
    public void setValue(T value) {
        this.value = value;
    }

    /**
     * Returns the hash code for this node.
     * @return The hash code for this node.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.mnemonic);
        hash = 67 * hash + Objects.hashCode(this.name);
        return hash;
    }

    /**
     * Returns true if this node is equal to the specified node; otherwise false.
     * @param obj The node to compare this node to.
     * @return True if this node is equal to the specified node; otherwise false.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Node<?> other = (Node<?>) obj;
        if (!Objects.equals(this.mnemonic, other.mnemonic)) {
            return false;
        }
        return Objects.equals(this.name, other.name);
    }
}
