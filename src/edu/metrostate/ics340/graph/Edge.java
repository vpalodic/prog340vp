/**
 * File: Edge.java
 */
package edu.metrostate.ics340.graph;

import java.util.Objects;

/**
 * <p>
 Edge class for ICS 340 Programming Assignment that represents a directed
 edge from source node to target node in a graph.
 * 
 * @author Vincent J Palodichuk <a HREF="mailto:hu0011wy@metrostate.edu">
 *         (e-mail me) </a>
 *
 * @version 01/12/2018
 * 
 * @param <T> The type for the value and label of the Node and Edge respectively.
 * 
 */
public class Edge<T extends Comparable<?>> {
    private Node<T> source;
    private Node<T> target;
    private T label;

    /**
     * Constructs an empty Edge.
     */
    public Edge() {
        this(null, null, null);
    }
    
    /**
     * Constructs an edge from  the source node to the target node
     * with the specified label.
     * @param from The source node of this edge.
     * @param to The target node of this edge.
     * @param label The label for this edge.
     */
    public Edge(Node<T> from, Node<T> to, T label) {
        this.source = from;
        this.target = to;
        this.label = label;
    }

    /**
     * Returns the source node of this edge.
     * @return The source node of this edge.
     */
    public Node<T> getSource() {
        return source;
    }

    /**
     * Returns the target node of this edge.
     * @return The target node of this edge.
     */
    public Node<T> getTarget() {
        return target;
    }

    /**
     * Returns the label of this edge.
     * @return The label of this edge.
     */
    public T getLabel() {
        return label;
    }

    /**
     * Sets the new source node of this edge.
     * @param source The new source node of this edge.
     */
    public void setSource(Node<T> source) {
        this.source = source;
    }

    /**
     * Sets the new target node of this edge.
     * @param target The new target node of this edge.
     */
    public void setTarget(Node<T> target) {
        this.target = target;
    }

    /**
     * Sets the new label of this edge.
     * @param label The new label of this edge.
     */
    public void setLabel(T label) {
        this.label = label;
    }

    /**
     * Returns the hash code of this edge.
     * @return The hash code of this edge.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.source);
        hash = 53 * hash + Objects.hashCode(this.target);
        return hash;
    }

    /**
     * Returns true if this edge is equal to the specified edge; otherwise false.
     * Two edges are considered equal if they have the same source and target nodes.
     * 
     * @param obj The edge to compare this edge to.
     * @return True if this edge is equal target the specified edge; otherwise false.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Edge<?> other = (Edge<?>) obj;
        if (!Objects.equals(this.source, other.source)) {
            return false;
        }
        return Objects.equals(this.target, other.target);
    }
}
