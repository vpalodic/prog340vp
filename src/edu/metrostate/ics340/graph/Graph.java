/**
 * File: Graph.java
 */
package edu.metrostate.ics340.graph;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>
 * Graph class for ICS 340 Programming Assignment. A Graph consists of
 * Nodes and Edges that connect those nodes.
 * 
 * @author Vincent J Palodichuk <a HREF="mailto:hu0011wy@metrostate.edu">
 *         (e-mail me) </a>
 *
 * @version 01/12/2018
 * @param <T> The type of the value and label for a Node and Edge respectively.
 *
 */
public class Graph<T extends Comparable<?>> {
    private static final String DELIMITER = "\\s+"; // one or more spaces.
    private static final String NO_VALUE = "~";
    private static final int START_INDEX = 2;
    private static final int NAME_INDEX = 0;
    private static final int VALUE_INDEX = 1;
    private static final String DEFAULT_NAME = "graph";
    private final Map<String, Node<T>> nodes;
    private String name;
    
    /**
     * Construct an empty graph using the default name.
     */
    public Graph() {
        this(DEFAULT_NAME);
    }
    
    /**
     * Construct an empty graph with the specified name.
     * @param name The name for this graph
     */
    public Graph(String name) {
        nodes = new HashMap<>();
        this.name = name;
    }
    
    /**
     * Returns a Collection of Node objects that are in this graph.
     * 
     * @return the java.util.Collection&lt;edu.metrostate.ics340.graph.Node&lt;T&gt;&gt;
     */
    public Collection<Node<T>> getNodes() {
        return nodes.values();
    }

    /**
     * Returns a set of all the mnemonics for the nodes in this graph.
     * @return the java.util.Set&lt;java.lang.String&gt; 
     */
    public Set<String> getNodeMnemonics() {
        return nodes.keySet();
    }
    
    /**
     * Returns the name of this graph.
     * @return The name of this graph.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the new name for this graph.
     * @param name The new name of this graph.
     */
    public void setName(String name) {
        this.name = name;
    }    

    /**
     * Adds a new Node to this graph identified by the specified mnemonic. Returns
     * true if the node was added to this graph.
     * 
     * @param mnemonic The mnemonic that identifies the new Node in this graph.
     * @return True if the node was added to this graph; otherwise false.
     */
    public boolean addNode(String mnemonic) {
        boolean answer = false;
        
        if (!nodes.containsKey(mnemonic)) {
            answer = addNode(new Node<>(mnemonic));
        }
        
        return answer;
    }

    /**
     * Adds a new Node to this graph identified by the specified node's mnemonic. Returns
     * true if the node was added to this graph.
     * 
     * @param node The new Node to add to this graph.
     * @return True if the node was added to this graph; otherwise false.
     */
    public boolean addNode(Node<T> node) {
        boolean answer = false;
        
        if (node != null && node.getMnemonic() != null && !nodes.containsKey(node.getMnemonic())) {
            answer = nodes.put(node.getMnemonic(), node) == null;
        }
        
        return answer;
    }

    /**
     * Removes all edges from and to the specified node and removes it from this graph.
     * Returns a reference to the node that was removed. If the specified node is not
     * in this graph, null is returned.
     * 
     * @param mnemonic The identifier for the node to remove.
     * @return Returns a reference to the node that was removed with no edges.
     * If the specified node is not in this graph, null is returned.
     */
    public Node<T> removeNode(String mnemonic) {
        Node<T> answer = null;
        
        if (nodes.containsKey(mnemonic)) {
            getEdgesTo(mnemonic).forEach((edge) -> removeEdge(edge));
            nodes.get(mnemonic).removeEdges();
            answer = nodes.remove(mnemonic);
        }
        
        return answer;
    }
    
    /**
     * Removes all edges from and to the specified node and removes it from this graph.
     * Returns a reference to the node that was removed. If the specified node is not
     * in this graph, null is returned.
     * 
     * @param node The specified node to remove.
     * @return Returns a reference to the node that was removed with no edges.
     * If the specified node is not in this graph, null is returned.
     */
    public Node<T> removeNode(Node<T> node) {
        Node<T> answer = null;
        
        if (node != null) {
            answer = removeNode(node.getMnemonic());
        }
        
        return answer;
    }
    
    /**
     * Removes all edges from all nodes and removes all of the nodes from this
     * graph.
     */
    public void removeNodes() {
        removeEdges();
        nodes.clear();
    }
    
    /**
     * Adds a new edge to this graph. Both source and target nodes must already
     * be in the graph. Returns true if the edge was added to this graph; otherwise
     * false is returned.
     * @param source The source node mnemonic.
     * @param target The target node mnemonic.
     * @param label The label of the edge.
     * @return True if the edge was added to this graph; otherwise false.
     */
    public boolean addEdge(String source, String target, T label) {
        boolean answer = false;
        
        if (nodes.containsKey(source) && nodes.containsKey(target)) {
            answer = addEdge(nodes.get(source), nodes.get(target), label);
        }
        
        return answer;
    }
    
    /**
     * Adds a new edge to this graph. Both source and target nodes must already
     * be in the graph. Returns true if the edge was added to this graph; otherwise
     * false is returned.
     * @param source The source node.
     * @param target The target node.
     * @param label The label of the edge.
     * @return True if the edge was added to this graph; otherwise false.
     */
    public boolean addEdge(Node<T> source, Node<T> target, T label) {
        boolean answer = false;
        
        if (source != null && target != null) {
            answer = source.addEdge(target, label);
        }
        
        return answer;
    }
    
    /**
     * Returns the Node with the specified mnemonic.
     * @param mnemonic The identifier for the Node in this graph.
     * @return The Node with the specified mnemonic or null if there is no Node
     * with that mnemonic..
     */
    public Node<T> getNode(String mnemonic) {
        Node<T> answer = null;
        
        if (nodes.containsKey(mnemonic)) {
            answer = nodes.get(mnemonic);
        }
        
        return answer;
    }

    /**
     * Returns the number of Nodes in this graph.
     * @return The number of Nodes in this graph.
     */
    public int getNumNodes() {
        return nodes.size();
    }

    /**
     * Calculates and returns the total number of Edges in this graph.
     * @return The total number of Edges in this graph.
     */
    public int getNumEdges() {
        int answer = 0;
        
        answer = nodes.values().stream().map((node) -> node.getNumEdges()).reduce(answer, Integer::sum);
        
        return answer;
    }

    /**
     * Returns a List of Edges that connect to the specified Node; which includes
     * any self edges.
     * 
     * @param mnemonic The mnemonic for the Node that the edges need to connect to.
     * @return the java.util.Set&lt;edu.metrostate.ics340.graph.Edge&lt;T&gt;&gt; containing
     * the requested edges.
     */
    public Set<Edge<T>> getEdgesTo(String mnemonic) {
        Set<Edge<T>> answer = null;
        
        if (nodes.containsKey(mnemonic)) {
            answer = getEdgesTo(nodes.get(mnemonic));
        } else {
            answer = new HashSet<>();
        }
        
        return answer;
    }

    /**
     * Returns a List of Edges that connect to the specified Node; which includes
     * any self edges.
     * 
     * @param target The Node that the edges need to connect to.
     * @return the java.util.Set&lt;edu.metrostate.ics340.graph.Edge&lt;T&gt;&gt; containing
     * the requested edges.
     */
    public Set<Edge<T>> getEdgesTo(Node<T> target) {
        final Set<Edge<T>> answer = new HashSet<>();
        
        nodes.values()
                .forEach((node) -> {
                    node.getEdges()
                            .stream()
                            .filter((edge) -> (Objects.equals(edge.getTarget(), target) == true))
                            .forEach((edge) -> answer.add(edge));
                });
        
        return answer;
    }
    
    /**
     * Returns a set of edges for the specified node.
     * @param mnemonic The identifier of the node.
     * @return A java.util.Set&lt;edu.metrostate.ics340.graph.Edge&lt;T&gt;&gt; containing
     * the requested edges.
     */
    public Set<Edge<T>> getEdges(String mnemonic) {
        Set<Edge<T>> answer = null;
        
        if (nodes.containsKey(mnemonic)) {
            answer = nodes.get(mnemonic).getEdges();
        } else {
            answer = new HashSet<>();
        }
        
        return answer;
    }
    
    /**
     * Returns a set of edges for the specified node.
     * @param source The node to retrieve the edges from.
     * @return A java.util.Set&lt;edu.metrostate.ics340.graph.Edge&lt;T&gt;&gt; containing
     * the requested edges.
     */
    public Set<Edge<T>> getEdges(Node<T> source) {
        Set<Edge<T>> answer = null;
        
        if (source != null) {
            answer = getEdges(source.getMnemonic());
        } else {
            answer = new HashSet<>();
        }
        
        return answer;
    }
    
    /**
     * Constructs and returns a new edu.metrostate.ics340.graphGraph&lt;java.lang.String&gt;
     * from the graph contained within the specified file. Null is returned if the specified
     * file is not a valid graph file or if an error occurs.
     * 
     * @param selectedFile The java.io.File to read the graph from.
     * @return the edu.metrostate.ics340.graph.Graph&lt;java.lang.String&gt;
     */
    public static Graph<String> stringGraphFromFile(File selectedFile) {
        if (selectedFile == null) {
            throw new IllegalStateException("A file must be selected in order to activate this method.");
        }
        
        Graph<String> graph = null;
        
        try (BufferedReader br = new BufferedReader(new FileReader(selectedFile));) {
            String headerLine = br.readLine();
            
            if (headerLine != null && headerLine.charAt(0) == '~') {
                String name = selectedFile.getName();
                int index = name.lastIndexOf('.');
                
                if (index > 0) {
                    name = name.substring(0, index);
                }
                
                graph = new Graph<>(name);
                String[] header = headerLine.split(DELIMITER);
                
                for (int i = START_INDEX; i < header.length; i++) {
                    graph.addNode(header[i]);
                }
                
                String line;
                int count = 0;
                
                while ((line = br.readLine()) != null) {
                    String[] row = line.split(DELIMITER);
                    String source = header[START_INDEX + count++];
                    Node<String> node = graph.getNode(source);

                    node.setName(row[NAME_INDEX]);
                    node.setValue(Objects.equals(row[VALUE_INDEX], NO_VALUE) ? null : row[VALUE_INDEX]);
                    
                    for (int i = START_INDEX; i < row.length; i++) {
                        if (!Objects.equals(row[i], NO_VALUE)) {
                            graph.addEdge(source, header[i], row[i]);
                        }
                    }
                }
            }            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Graph.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Graph.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return graph;
    }

    /**
     * Removes the specified edge from this graph. Returns true if the edge was
     * in this graph and was successfully removed from it; Otherwise false.
     * @param edge The edge to remove from this graph.
     * @return true if the edge was in this graph and was successfully removed
     * from it; Otherwise false.
     */
    public boolean removeEdge(Edge<T> edge) {
        boolean answer = false;
        
        if (edge != null) {
            answer = removeEdge(edge.getSource(), edge.getTarget());
        }
        
        return answer;
    }

    /**
     * Removes the specified edge from this graph. Returns true if the edge was
     * in this graph and was successfully removed from it; Otherwise false.
     * @param source The source node of the edge to remove from this graph.
     * @param target The target node of the edge to remove from this graph.
     * @return true if the edge was in this graph and was successfully removed
     * from it; Otherwise false.
     */
    public boolean removeEdge(Node<T> source, Node<T> target) {
        boolean answer = false;
        
        if (source != null && target != null) {
            answer = removeEdge(source.getMnemonic(), target.getMnemonic());
        }
        
        return answer;
    }
    
    /**
     * Removes the specified edge from this graph. Returns true if the edge was
     * in this graph and was successfully removed from it; Otherwise false.
     * @param source The source node's mnemonic of the edge to remove from this graph.
     * @param target The target node's mnemonic of the edge to remove from this graph.
     * @return true if the edge was in this graph and was successfully removed
     * from it; Otherwise false.
     */
    public boolean removeEdge(String source, String target) {
        boolean answer = false;
        
        if (nodes.containsKey(source) && nodes.containsKey(target)) {
            answer = nodes.get(source).removeEdge(nodes.get(target)) != null;
        }
        
        return answer;
    }
    
    /**
     * Removes all edges from all nodes that are in this graph.
     */
    public void removeEdges() {
        nodes.values().forEach((node) -> node.removeEdges());
    }
}
