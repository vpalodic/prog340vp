package edu.metrostate.ics340.graph;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author vpalo
 */
public class GraphTest {
    public GraphTest() {
    }
    
    /**
     * Test of getNodes method, of class Graph.
     */
    @Test
    public void testGetNodes() {
        System.out.println("getNodes");
        Graph<String> instance = new Graph<>();
        Collection<Node<String>> expResult = new LinkedList<>();
        Collection<Node<String>> result = instance.getNodes();
        assertEquals(expResult.size(), result.size());
    }

    /**
     * Test of getName method, of class Graph.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Graph<String> instance = new Graph<>();
        String expResult = "graph";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setName method, of class Graph.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "";
        Graph<String> instance = new Graph<>();
        instance.setName(name);
    }

    /**
     * Test of addNode method, of class Graph.
     */
    @Test
    public void testAddNode() {
        System.out.println("addNode");
        String mnemonic = "";
        Graph<String> instance = new Graph<>();
        boolean expResult = true;
        boolean result = instance.addNode(mnemonic);
        assertEquals(expResult, result);
        assertEquals(1, instance.getNumNodes());
    }

    /**
     * Test of getNode method, of class Graph.
     */
    @Test
    public void testGetNode() {
        System.out.println("getNode");
        String mnemonic = "";
        Graph<String> instance = new Graph<>();
        Node<String> expResult = null;
        Node<String> result = instance.getNode(mnemonic);
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumNodes method, of class Graph.
     */
    @Test
    public void testGetNumNodes() {
        System.out.println("getNumNodes");
        Graph<String> instance = new Graph<>();
        int expResult = 0;
        int result = instance.getNumNodes();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumEdges method, of class Graph.
     */
    @Test
    public void testGetNumEdges() {
        System.out.println("getNumEdges");
        Graph<String> instance = new Graph<>();
        int expResult = 0;
        int result = instance.getNumEdges();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEdgesTo method, of class Graph.
     */
    @Test
    public void testGetEdgesTo() {
        System.out.println("getEdgesTo");
        Graph<String> instance = new Graph<>();
        Set<Edge<String>> expResult = new HashSet<>();
        Set<Edge<String>> result = instance.getEdgesTo("");
        assertEquals(expResult, result);
    }
    
}
